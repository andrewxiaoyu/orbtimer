const remote = require('electron').remote;
const electron = remote.getCurrentWindow();

function closeWindow() {
    electron.close();
}

function minimizeWindow() {
    electron.minimize();
}

function maximizeWindow() {
    electron.isMaximized() ? electron.unmaximize() : electron.maximize();
}